Meteor.publish("thoughts", function () {
  return Thoughts.find({owner: this.userId, removed: {$ne:true}});
});
