Template.thoughts_by_day.onCreated(function () {
  const template = this;
  template.loaded = new ReactiveVar(false)
  template.subscribe("thoughts", function () {
    template.loaded.set(true);
  });

  // get today's thoughts starting at midnight
  let today = new Date();
  today.setHours(0);
  today.setMinutes(0);
  today.setSeconds(0);

  template.today = today;

});

Template.thoughts_by_day.helpers({
  dataLoaded() {
    return Template.instance().loaded.get();
  },

  day() {
    return moment().format("dddd, MMM DD");
  },

  sortThoughtsOptions() {
    return {
      sortField: "created_at",
      group: {
        name: "thoughts",
        pull: true,
        put: true
      },
      handle: ".fa-bars",
      sort: false,
      onAdd: function(e) {
        Meteor.call("updateThoughtType", e.data._id, THOUGHT_TYPES.THOUGHT);
      }
    }
  },
  sortTasksOptions() {
    return {
      sortField: "created_at",
      group: {
        name: "thoughts",
        pull: true,
        put: true
      },
      handle: ".fa-bars",
      sort: false,
      onAdd: function(e) {
        Meteor.call("updateThoughtType", e.data._id, THOUGHT_TYPES.TASK);
      }
    }
  },
  sortEventsOptions() {
    return {
      sortField: "created_at",
      group: {
        name: "thoughts",
        pull: true,
        put: true
      },
      handle: ".fa-bars",
      sort: false,
      onAdd: function(e) {
        Meteor.call("updateThoughtType", e.data._id, THOUGHT_TYPES.EVENT);
      }
    }
  },

  thoughts() {
    // query thoughts
    let today = Template.instance().today;
    return Thoughts.find({
      type: THOUGHT_TYPES.THOUGHT,
      created_at: { $gte: today }
    }, {
      sort: {created_at: 1}
    });
  },
  tasks() {
    // query thoughts
    let today = Template.instance().today;
    return Thoughts.find({
      type: THOUGHT_TYPES.TASK,
      created_at: { $gte: today }
    }, {
      sort: {is_completed: 1, created_at: 1}
    });

  },
  events() {
    // query thoughts
    let today = Template.instance().today;
    return Thoughts.find({
      type: THOUGHT_TYPES.EVENT,
      created_at: { $gte: today }
    }, {
      sort: {created_at: 1}
    });

  }
});

Template.thoughts_by_day.events({
  "submit .add-thought-form": function (e, t) {
    const type = _.find(e.target.add_thought_type, (option) => {
      return option.checked;
    }).value;
    const content = e.target.add_thought_input.value;
    const owner = Meteor.userId();

    const thought = { type, content, owner };

    Meteor.call("addThought", thought, function (err, res) {
      if (err) $('#error-message').html(err.reason);

      e.target.add_thought_input.value = "";
    });

    return false;
  },

  "click .remove-thought": function (e, t) {
    Meteor.call("removeThought", this._id, function (err, res) {
      if (err) $('#error-message').html(err.reason);
    });
  },

  "click .complete-thought, click .uncomplete-thought": function (e,t) {
    Meteor.call('toggleThought', this._id, this.is_completed, function (err, res) {
      if (err) $('#error-message').html(err.reason);
    });
  },

  "input .add-thought-input": _.debounce(function (e, t) {
    const predictedType = Meteor.call('predictType', e.target.value);
    console.log(predictedType);
    t.$('.add-thought-type')[! predictedType ? THOUGHT_TYPES.THOUGHT : predictType].checked = true;
  }, 1000)


});
