Meteor.methods({

  addThought(thought, callback) {
    if (Meteor.isServer) Thoughts.insert(thought, callback);
  },

  removeThought(thoughtId) {
    if (!thoughtId) return "insufficient params (thoughtId)";
    Thoughts.update(thoughtId, {$set: { removed: true }});
  },

  toggleThought(thoughtId, isCompleted, callback) {
    Thoughts.toggle(thoughtId, isCompleted, callback);
  },

  updateThoughtType(thoughtId, type) {
    if (!thoughtId || !type) return "insufficient params (thoughtId, type)";
    Thoughts.updateThoughtType(thoughtId, type);
  },

  predictType(thought) {
    if (Meteor.isServer) {
      const prediction = WordsByType.predictType(thought);
      return prediction;
    }
  }

  letsAddChanges() {
    let thisIsAChangedFile = "sup diff tool";

    return thisIsAChangedFile;
  }

});
