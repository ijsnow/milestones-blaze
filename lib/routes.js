FlowRouter.route("/", {
    name: "home",
    action: () => {
        FlowRouter.go("thoughts_by_day")
    }
})

FlowRouter.route("/day", {
    name: "thoughts_by_day",
    action: () => {
      BlazeLayout.render("layout", {
        header: "navigation_bar",
        content: "thoughts_by_day"
      })
    }
})
