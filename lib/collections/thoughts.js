SimpleSchema.debug = true;

class ThoughtsCollection extends Mongo.Collection {
  /**
   * Hook for the collections insert method. Used in this.remember
   * Used to set up the initial word object correctly.
   * @method
   * @param {Object} thought - The thought we are adding.
   * @param {Function} callback - The callback to be called when the thought has been inserted.
   * @returns void
   */
  insert(thought, callback) {

    if (!thought.type || !thought.owner || !thought.content) {
      callback(new Meteor.Error(
        "not-enough-data", "Not enough information to submit a thought."
      ));
    }

    const isCompletedExtendor = thought.type !== THOUGHT_TYPES.THOUGHT ?
      { is_completed: false } :
      {};

    const entityThought = _.extend(thought, {
      type: parseInt(thought.type),
      created_at: new Date(),
    }, isCompletedExtendor);

    super(entityThought, callback);
    WordsByType.remember(entityThought.content, entityThought.type);
  }
  /**
   * Update the type of an ID
   * @method
   * @param {Mongo ID} id - ID of the thought to update.
   * @param {new} newType - The type we are changing the thought to.
   * @returns void
   */
  updateThoughtType(id, newType) {
    super(thoughtId, {$set: { type: type }});

    const { content, type } = this.findOne({ _id: id });

    WordsByType.remember(content, newType, type);
  }

  toggle(thoughtId, isCompleted, callback) {
    this.update(thoughtId, { $set: { is_completed: !isCompleted }}, (err) => {
      if (err) callback(err);
    });
  }
}

Thoughts = new ThoughtsCollection("thoughts");

Thoughts.allow({
  insert: function (userId, attribute) {
    return true;
  },
  update: function (userId, attribute) {
    return true;
  },
  remove: function (userId, attribute) {
    return true;
  }
});

const ThoughtsSchema = new SimpleSchema({
  "created_at": {
    type: Date,
    label: "Time/date thought was created",
    optional: false
  },
  /**
   * Will be mapped with an enum
   * 1 = "thought"
   * 2 = "task"
   * 3 = "event"
   */
  "type": {
    type: Number,
    label: "Type of thought",
    optional: false
  },
  "owner": {
    type: String,
    label: "Owner of thought",
    optional: true
  },
  "content": {
    type: String,
    label: "Content of thought",
    optional: false
  },
  "attachments": {
    type: String,
    label: "Attachments of thought",
    optional: true
  },
  "is_completed": {
    type: Boolean,
    label: "Whether or not thought is complete",
    optional: true,
  },
  "event_time": {
    type: Date,
    label: "Time/deadline of thought",
    optional: true
  },
  "removed": {
    type: Boolean,
    label: "Whether the thought has been removed by the user",
    optional: true
  }
});

Thoughts.attachSchema(ThoughtsSchema);
