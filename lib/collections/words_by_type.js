let Wordpos = null;
if (Meteor.isServer) {
  // Constructor for the wordpos package used for parsing the thoughts.
  Wordpos = Meteor.npmRequire('wordpos');
}
/*
 * WordsByType will be used to remember what words the user has used for
 * certain thought types. We will use this to predict what type of thought
 * someone has entered.
 */
class WordsByTypeCollection extends Mongo.Collection {
  /**
   * Used just like the constructor of Mongo.Collection.
   * Adds a few helper fields
   * @constructor
   * @param {String} coll - Name for Mongo Collection just like Mongo.Collection takes
   */
  constructor(coll) {
    super(coll);

    this.typesArr = [
      { field: 'event_count', type: THOUGHT_TYPES.EVENT },
      { field: 'thought_count', type: THOUGHT_TYPES.THOUGHT },
      { field: 'task_count', type: THOUGHT_TYPES.TASK }
    ];

    if (Meteor.isServer) {
      this._wordpos = new Wordpos();
    }
  }
   /**
    * If the word has not been used before, add a new entry in the collection,
    * If it has, increment the counter for this thought type and
    * decrement the one that it was.
    * Adds a few helper fields
    * @method
    * @param {String} text - The text that was added that we want to remember
    * @param {Number} type - Type of the thought that was added. From THOUGHT_TYPES.
    * @param {Number} oldType - The type that the thought was if this was being called by an update
    * @returns void
    */
  remember(text, type, oldType) {
    if (Meteor.isClient) return;

    const words = this._wordpos.parse(text);

    words.forEach((word) => {
      const wordEntity = this.findOne({ word: word.toLowerCase() });

      if (wordEntity) {
        this.update(wordEntity, type, oldType);
      } else {
        this.insert(word.toLowerCase(), type);
      }
    });

    console.log(`Predicted type for: ${text}`);
    const p = WordsByType.predictType(text);
    console.log(p === 0 ? 'thought' : ((p === 1) ? 'task' : 'event'));
  }
  /**
   * Function used to predict the type of a thought.
   * @method
   * @param {String} text - The text that we are predicting the type of
   * @returns {Number} The value of the type according to THOUGHT_TYPES
   */
  predictType(text) {
    if (Meteor.isClient) {
      console.log('Predicting type...');
      // Just default to a thought for the client.
      return THOUGHT_TYPES.THOUGHT;
    }
    // Split into individual words.
    /** https://www.npmjs.com/package/wordpos#parsetext **/
    const words = this._wordpos.parse(text);

    // Get the type prediction for each word.
    const predictions = words.map((word) => {
      return this.predictTypeForWord(word);
    });

    // Create an array that will keep
    // track of the amount of predictions for each type.
    let typeCount = [];
    typeCount[THOUGHT_TYPES.THOUGHT] = 0;
    typeCount[THOUGHT_TYPES.TASK] = 0;
    typeCount[THOUGHT_TYPES.EVENT] = 0;

    // Count the number of predictions for each type.
    const counts = _.reduce(predictions, (memo, type) => {
      memo[type] = memo[type] + 1;
      return memo;
    }, typeCount);

    // Get the type with the most predictions.
    // Not sure what we want to do about predictions that tie.
    const maxCount = _.max(counts);

    // Return the value of the type according to THOUGHT_TYPES
    return _.indexOf(counts, maxCount);
  }

  /**
   * Function used to get the type that this word is used most in.
   * Currently only being used internally by this.predictType
   * @method
   * @param {String} text - The word that we are predicting the type of
   * @returns {Number} The value of the type according to THOUGHT_TYPES
   */
  predictTypeForWord(word) {
    const wordEntity = this.findOne({ word: word.toLowerCase() });
    // Default to thought for words we don't have yet.
    if (! wordEntity) return THOUGHT_TYPES.THOUGHT;
    /*
     * Returns the type of the highest of the following:
     * wordEntity.event_count,
     * wordEntity.thought_count,
     * wordEntity.task_count
     */
    return _.reduce(this.typesArr, (memo, type) => {
      if (wordEntity[type.field] >= wordEntity[memo.field]) {
        return type;
      }

      return memo;
    }).type;
  }
  /**
   * Hook for the collections insert method. Used in this.remember
   * Used to set up the initial word object correctly.
   * @method
   * @param {String} word - The word that we are remembering.
   * @param {Number} type - The type of the thought the word was used in.
   * @returns void
   */
  insert(word, type) {
    let wordEntity = { word };

    switch (type) {
      case THOUGHT_TYPES.EVENT:
        wordEntity.event_count = 1;
        break;
      case THOUGHT_TYPES.TASK:
        wordEntity.task_count = 1;
        break;
      case THOUGHT_TYPES.THOUGHT:
        wordEntity.thought_count = 1;
        break;
      default:
        break; // Shouldn't ever get here.
    }

    super(wordEntity);
  }
  /**
   * Hook for the collections update method. Used in this.remember
   * Used to increment the new type's count and decrement the old type's count.
   * @method
   * @param {String} word - The word that we are remembering.
   * @param {Number} type - The type of the thought the word was used in.
   * @param {Number} oldType - The type of the thought the word was originally used in.
   * @returns void
   */
  update(wordEntity, type, oldType) {
    const query = { _id: wordEntity._id };
    let modifier = { $set: {} };

    switch (type) {
      case THOUGHT_TYPES.EVENT:
        modifier.$set.event_count = wordEntity.event_count + 1;
        break;
      case THOUGHT_TYPES.TASK:
        modifier.$set.task_count = wordEntity.task_count + 1;
        break;
      case THOUGHT_TYPES.THOUGHT:
        modifier.$set.thought_count = wordEntity.thought_count + 1;
        break;
      default:
        break;
    }

    switch (oldType) {
      case THOUGHT_TYPES.EVENT:
        modifier.$set.event_count = wordEntity.event_count - 1;
        break;
      case THOUGHT_TYPES.TASK:
        modifier.$set.task_count = wordEntity.task_count - 1;
        break;
      case THOUGHT_TYPES.THOUGHT:
        modifier.$set.thought_count = wordEntity.thought_count - 1;
        break;
      default:
        break;
    }

    super(query, modifier);
  }
};

WordsByType = new WordsByTypeCollection('words_by_type');

const WordsByTypeSchema = new SimpleSchema({
  "created_at": {
    type: Date,
    label: "Time/date thought was created",
    optional: false,
    autoValue: () => {
      return new Date();
    }
  },
  "word": {
    type: String,
    label: "Word we are remembering so that we can predict the type in the future"
  },
  "event_count": {
    type: Number,
    optional: false,
    defaultValue: 0
  },
  "thought_count": {
    type: Number,
    optional: false,
    defaultValue: 0
  },
  "task_count": {
    type: Number,
    optional: false,
    defaultValue: 0
  }
});

WordsByType.attachSchema(WordsByTypeSchema);
